Here you can find the links to different projects created during the complete course. For each lecture there is a repository for the samples shown throughout the slides, and a repository containing a completed solution of the lab.

| Lecture | Topic  | Lecture Project | Lab Solution |
|--|--|--|--|
| Lecture 2 | XAML  | https://bitbucket.org/naii_windows/lecture02 | https://bitbucket.org/naii_windows/lecture02_lab | 
| Lecture 3 | MVVM  | https://bitbucket.org/naii_windows/lecture03 | https://bitbucket.org/naii_windows/lecture03_lab | 
| Lecture 4 | Services & API  | https://bitbucket.org/naii_windows/lecture04 | https://bitbucket.org/naii_windows/lecture04_lab | 
| Lecture 5 | Windows 10 Experience  | https://bitbucket.org/naii_windows/lecture05 | https://bitbucket.org/naii_windows/lecture05_lab | 
| Lecture 6 | Sensors  | https://bitbucket.org/naii_windows/lecture06 | https://bitbucket.org/naii_windows/lecture06_lab | 
| Lecture 7 | Mixed Reality  | | 
